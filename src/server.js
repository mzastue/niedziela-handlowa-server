const express = require('express');
const expressGraphQL = require('express-graphql');
const dotenv = require('dotenv').config();
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
const models = require('./models/index');
const schema = require('./schema/index');
const PORT = process.env.PORT || 4000;

app.use('/graphql', cors(), bodyParser.json(), expressGraphQL({
  schema,
  graphiql: process.env.MODE === 'development',
}));

app.use(function (req, res) {
  res.status(404).send("404. Sorry.")
});

app.listen(PORT, () => {
  console.log(`Listening on PORT ${PORT}`);
});
