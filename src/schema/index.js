const { GraphQLSchema } = require('graphql');
const query = require('./rootQueryType');

module.exports = new GraphQLSchema({
  query
});
