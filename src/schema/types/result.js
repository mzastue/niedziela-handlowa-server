const {
  GraphQLID,
  GraphQLString,
  GraphQLObjectType,
  GraphQLBoolean,
  GraphQLInt
} = require('graphql');
const _ = require('lodash');

module.exports = new GraphQLObjectType({
  name: 'Result',
  fields: () => ({
    currentDay: { type: GraphQLString },
    nextSunday: { type: GraphQLString },
  }),
})