const {
  GraphQLID,
  GraphQLString,
  GraphQLObjectType,
} = require('graphql');
const _ = require('lodash');

module.exports = new GraphQLObjectType({
  name: 'Day',
  fields: () => ({
    id: { type: GraphQLID },
    date: { type: GraphQLString },
  }),
});