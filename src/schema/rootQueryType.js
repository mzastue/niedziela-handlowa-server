const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLScalarType,
} = require('graphql');
const mongoose = require('mongoose');
const DayType = require('./types/day');
const Day = mongoose.model('day');
const ResultType = require('./types/result');
const Result = mongoose.model('result');

module.exports = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: () => ({
    sunday: {
      type: DayType,
      resolve (root, args) {
        const date = new Date().toISOString().slice(0, 10);
        return Day.getSunday(date);
      }
    },
    result: {
      type: ResultType,
      resolve () {
        return Result.get();
      }
    }
  })
});
