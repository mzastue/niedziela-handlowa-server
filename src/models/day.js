const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');

const sundays = require('../mockData/sundays.json');

const DaySchema = new Schema({
  id: { type: String },
  date: { type: String },
});

DaySchema.statics.getSunday = day => {
  return _.find(sundays.sundays, sunday => sunday === day);
};

mongoose.model('day', DaySchema);
