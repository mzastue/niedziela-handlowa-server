const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const moment = require('moment');
const _ = require('lodash');

const sundays = require('../mockData/sundays.json');

moment.updateLocale('pl', {
  weekdays : [
      "Niedziela", "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota"
  ],
  month: [
    "Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"
  ]
});

const ResultSchema = new Schema({
  data: { type: String },
  day: { type: String },
  month: { type: String },
  is_sunday: { type: Boolean },
  is_shopping: { type: Boolean },
  days_to_next_non_shopping_sunday: { type: Number },
  date_label: { type: String }
});

const getDaysToNextNonShoppingSunday = date => {
  const higherThanZero = item => item > 0;
  const start = moment(date);
  return sundays.sundays
    .map(sunday => {
      const end = moment(sunday);
      const timeDiff = Math.abs(end.valueOf() - start.valueOf());
      return Math.ceil(timeDiff / (1000 * 3600 * 24)) - 1;
    })
    .filter(higherThanZero)[0];
};

const getNextSunday = (date) => { 
  const SUNDAY_DAY_NUMBER = 7;
  date = moment(date);
  const dayNumber = date.day();
  const daysToSunday = SUNDAY_DAY_NUMBER - dayNumber;
  console.log(dayNumber, daysToSunday);
  return date.day(dayNumber + daysToSunday);
};

class DateObject {
  constructor (date) {
    const SUNDAY_DAY_NUMBER = 0;
    this.dayNumber = date.date();
    this.isSunday = date.day() === SUNDAY_DAY_NUMBER;
    this.dayLabel = date.format('dddd');
    this.monthName = date.format('MMMM');
    this.fullDate = date.format('YYYY-MM-DD');
    this.isShopping = this.isSunday
      ? _.find(sundays.sundays, sunday => sunday === date.format('YYYY-MM-DD')) === undefined
      : true
    ;
    this.toNextSunday = getDaysToNextNonShoppingSunday(date);
  }

  serialize () {
    return JSON.stringify(this);
  }
}

ResultSchema.statics.get = () => {
  const date = moment();
  const nextSunday = getNextSunday(date);

  return {
    currentDay: new DateObject(date).serialize(),
    nextSunday: new DateObject(nextSunday).serialize()
  };
};

mongoose.model('result', ResultSchema);
